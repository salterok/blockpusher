﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlockPusher
{
	enum Block : byte
	{
		Unknown = 0,
		Free = 1,
		Wall = 2,
		Box = 3,
		Place = 4
	}
}
