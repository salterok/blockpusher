﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlockPusher
{
	struct WorldRaw
	{
		public System.Drawing.Point size;
		public byte[] source;
		public Dictionary<System.Drawing.Point, Block> state;
	}
}
