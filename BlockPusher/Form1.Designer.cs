﻿namespace BlockPusher
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		private static global::System.Resources.ResourceManager resourceMan;

		private static global::System.Globalization.CultureInfo resourceCulture;

		[global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
		public static global::System.Resources.ResourceManager ResourceManager
		{
			get
			{
				if (object.ReferenceEquals(resourceMan, null))
				{
					global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("BlockPusher.Form1", typeof(Form1).Assembly);
					resourceMan = temp;
				}
				return resourceMan;
			}
		}

		[global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
		public static global::System.Globalization.CultureInfo Culture
		{
			get
			{
				return resourceCulture;
			}
			set
			{
				resourceCulture = value;
			}
		}

		public static System.Drawing.Bitmap player
		{
			get
			{
				object obj = ResourceManager.GetObject("player", resourceCulture);
				return ((System.Drawing.Bitmap)(obj));
			}
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.StopResume = new System.Windows.Forms.Button();
            this.stepsLabel = new System.Windows.Forms.Label();
            this.steps = new System.Windows.Forms.Label();
            this.timeLabel = new System.Windows.Forms.Label();
            this.time = new System.Windows.Forms.Label();
            this.Exit = new System.Windows.Forms.Button();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.saveFile = new System.Windows.Forms.SaveFileDialog();
            this.save = new wyDay.Controls.SplitButton();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuMain = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.createWorldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.contextMenu.SuspendLayout();
            this.contextMenuMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(454, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 30);
            this.button1.TabIndex = 1;
            this.button1.Text = "Load";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // StopResume
            // 
            this.StopResume.Location = new System.Drawing.Point(454, 67);
            this.StopResume.Name = "StopResume";
            this.StopResume.Size = new System.Drawing.Size(75, 30);
            this.StopResume.TabIndex = 2;
            this.StopResume.Text = "Stop";
            this.StopResume.UseVisualStyleBackColor = true;
            this.StopResume.Click += new System.EventHandler(this.StopResume_Click);
            // 
            // stepsLabel
            // 
            this.stepsLabel.AutoSize = true;
            this.stepsLabel.Location = new System.Drawing.Point(451, 150);
            this.stepsLabel.Name = "stepsLabel";
            this.stepsLabel.Size = new System.Drawing.Size(37, 13);
            this.stepsLabel.TabIndex = 3;
            this.stepsLabel.Text = "Steps:";
            // 
            // steps
            // 
            this.steps.AutoSize = true;
            this.steps.Location = new System.Drawing.Point(453, 182);
            this.steps.Name = "steps";
            this.steps.Size = new System.Drawing.Size(0, 13);
            this.steps.TabIndex = 4;
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(451, 220);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(73, 13);
            this.timeLabel.TabIndex = 5;
            this.timeLabel.Text = "Time elapsed:";
            // 
            // time
            // 
            this.time.AutoSize = true;
            this.time.Location = new System.Drawing.Point(451, 254);
            this.time.Name = "time";
            this.time.Size = new System.Drawing.Size(0, 13);
            this.time.TabIndex = 6;
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(454, 280);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(75, 31);
            this.Exit.TabIndex = 7;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // openFile
            // 
            this.openFile.DefaultExt = "bpf";
            this.openFile.Filter = "BlockPusher saves|*.bpf|All files|*.*";
            // 
            // saveFile
            // 
            this.saveFile.DefaultExt = "bpf";
            this.saveFile.Filter = "BlockPusher saves|*.bpf|All files|*.*";
            // 
            // save
            // 
            this.save.AutoSize = true;
            this.save.ContextMenuStrip = this.contextMenu;
            this.save.Location = new System.Drawing.Point(454, 112);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(75, 30);
            this.save.SplitMenuStrip = this.contextMenu;
            this.save.TabIndex = 8;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem});
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(121, 26);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(120, 22);
            this.saveAsToolStripMenuItem.Text = "Save As..";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // contextMenuMain
            // 
            this.contextMenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createWorldToolStripMenuItem});
            this.contextMenuMain.Name = "contextMenuMain";
            this.contextMenuMain.Size = new System.Drawing.Size(142, 26);
            // 
            // createWorldToolStripMenuItem
            // 
            this.createWorldToolStripMenuItem.Name = "createWorldToolStripMenuItem";
            this.createWorldToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.createWorldToolStripMenuItem.Text = "Create world";
            this.createWorldToolStripMenuItem.Click += new System.EventHandler(this.createWorldToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(408, 396);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(769, 430);
            this.ContextMenuStrip = this.contextMenuMain;
            this.Controls.Add(this.save);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.time);
            this.Controls.Add(this.timeLabel);
            this.Controls.Add(this.steps);
            this.Controls.Add(this.stepsLabel);
            this.Controls.Add(this.StopResume);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.contextMenu.ResumeLayout(false);
            this.contextMenuMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button StopResume;
        private System.Windows.Forms.Label stepsLabel;
        private System.Windows.Forms.Label steps;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.Label time;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.SaveFileDialog saveFile;
        private wyDay.Controls.SplitButton save;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuMain;
        private System.Windows.Forms.ToolStripMenuItem createWorldToolStripMenuItem;
	}
}

