﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BlockPusher
{
    public partial class Form1 : Form
    {
        Game game = new Game();
        Graphics graphic;
        Point blockSize;
        Image playerPicture;
        Image wallPicture;
        Image boxPicture;
        Image placePicture;
        Image freePicture;

        string filename;

        public Form1()
        {
            InitializeComponent();

            //var array = new byte[]
            //{ 
            //    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
            //    2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
            //    2, 1, 4, 3, 1, 3, 1, 1, 1, 2,
            //    2, 1, 1, 1, 1, 1, 1, 4, 1, 2,
            //    2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
            //    2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
            //    2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
            //    2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
            //    2, 1, 4, 3, 1, 3, 1, 1, 1, 2,
            //    2, 1, 1, 1, 1, 1, 1, 4, 1, 2,
            //    2, 1, 1, 1, 1, 1, 1, 1, 1, 2,
            //    2, 2, 2, 2, 2, 2, 2, 2, 2, 2
            //};
            //var world = new World(new Point(10, 12), array);
            //game = new Game(world, new Player() { X = 1, Y = 1, Name = "player" });

            //game.Save("level_3.bpf");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //var array = new byte[]
            //{ 
            //    2, 2, 2, 2, 2, 2, 2, 2,
            //    2, 1, 1, 1, 1, 1, 1, 2,
            //    2, 1, 4, 3, 1, 3, 1, 2,
            //    2, 1, 1, 1, 1, 1, 1, 2,
            //    2, 2, 2, 2, 2, 2, 2, 2
            //};
            //var world = new World(new Point(8, 5), array);
            //game = new Game(world, new Player() { X = 1, Y = 1, Name = "player" });
            OpenFile();
        }

        private void OpenFile()
        {
            game.Reset();
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                filename = openFile.FileName;
                game.Load(filename);
            }
            DrawWorld();

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (game.IsNotStarted)
            {
                game.Start();
            }
            if (game.IsStopped)
            {
                return;
            }
            switch (e.KeyChar)
            {
                case 'w':
                    game.Move(Direction.Up);
                    break;
                case 'd':
                    game.Move(Direction.Right);
                    break;
                case 's':
                    game.Move(Direction.Down);
                    break;
                case 'a':
                    game.Move(Direction.Left);
                    break;
            }
            DrawWorld();
            //////////////////////////////////////////////////////////////////////////
            time.Text = game.Time.Elapsed.ToString();
            steps.Text = game.player.Steps.ToString();
            //////////////////////////////////////////////////////////////////////////
            if (game.world.IsComplete)
            {
                MessageBox.Show(String.Format("Complete!\nYour score: {0}", game.Score));
                OpenFile();
            }
        }

        private void DrawWorld()
        {
            for (int y = 0; y < game.world.Size.Y; y++)
            {
                for (int x = 0; x < game.world.Size.X; x++)
                {
                    Draw(game.world.GetBlock(new Point(x, y)), x, y);
                }
            }
            graphic.DrawImage(playerPicture, new Rectangle(
                game.player.X * blockSize.X, game.player.Y * blockSize.Y,
                blockSize.X, blockSize.Y));
        }

        private void Draw(Block block, int x, int y)
        {
            SolidBrush brush = new SolidBrush(Color.White);
            Image picture;
            switch (block)
            {
                case Block.Box:
                    brush = new SolidBrush(Color.Brown);
                    picture = boxPicture;
                    break;
                case Block.Place:
                    brush = new SolidBrush(Color.Coral);
                    picture = placePicture;
                    break;
                case Block.Wall:
                    brush = new SolidBrush(Color.DarkGray);
                    picture = wallPicture;
                    break;
                case Block.Free:
                    brush = new SolidBrush(Color.Gray);
                    picture = freePicture;
                    break;
                default:
                    picture = freePicture;
                    break;
            }
            //graphic.FillRectangle(brush, new Rectangle(x * blockSize.X, y * blockSize.Y, (x + 1) * blockSize.X, (y + 1) * blockSize.Y));
            graphic.DrawImage(picture, new Rectangle(
                x * blockSize.X, y * blockSize.Y,
                blockSize.X, blockSize.Y));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            graphic = pictureBox1.CreateGraphics();
            blockSize = new Point(30, 30);
            playerPicture = new Bitmap(Resource1.player);
            wallPicture = new Bitmap(Resource1.wall);
            boxPicture = new Bitmap(Resource1.box);
            placePicture = new Bitmap(Resource1.place);
            freePicture = new Bitmap(Resource1.free);
        }

        private void StopResume_Click(object sender, EventArgs e)
        {
            if (game == null)
            {
                return;
            }
            if (game.IsStopped)
            {
                game.Start();
                (sender as Button).Text = "Stop";
            }
            else
            {
                game.Stop();
                (sender as Button).Text = "Resume";
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            game.Save();
            this.Close();
        }

        private void save_Click(object sender, EventArgs e)
        {
            game.Save(filename);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                game.Save(saveFile.FileName);
            }
        }

        private void createWorldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var creator = new WorldCreator();
            creator.ShowDialog();
            //
        }
    }
}
