﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace BlockPusher
{
	class World
	{
		public byte[] world { get; private set; }
		public Point Size { get; private set; }
		public Dictionary<Point, Block> boxes = new Dictionary<Point, Block>();
		private Point[] originalBoxPlace;
		private Point[] places;

		public World(Point point, byte[] array)
		{
			Size = point;
			world = array;
			var tempPlaces = new List<Point>();
			var originalBoxPlaceTemp = new List<Point>();
			for (int index = 0; index < array.Length; index++)
			{
				var pt = new Point(index % Size.X, index / Size.X);
				if ((Block)array[index] == Block.Box)
				{
					boxes.Add(pt, Block.Free);
					originalBoxPlaceTemp.Add(pt);
				}
				else if ((Block)array[index] == Block.Place)
				{
					tempPlaces.Add(pt);
				}
			}
			originalBoxPlace = originalBoxPlaceTemp.ToArray();
			places = tempPlaces.ToArray();
		}

		public Player MoveTo(Player player, Point destination)
		{
			if ((destination.X < 0 || destination.X >= Size.X) || (destination.Y < 0 || destination.Y >= Size.Y))
			{
				// fire if move out of map
				return player;
			}
			if (Math.Abs(player.X - destination.X) > 1 || Math.Abs(player.Y - destination.Y) > 1)
			{
				// will not fire with trusted caller
				return player;
			}
			var block = GetBlock(destination);
			if (block == Block.Wall)
			{
				return player;
			}
			if (block == Block.Free || block == Block.Place)
			{
                return player.Update(destination.X, destination.Y);
			}
			if (block == Block.Box)
			{
				var blockDestination = new Point(2 * destination.X - player.X, 2 * destination.Y - player.Y);
				var destinationBlock = GetBlock(blockDestination);
				if (destinationBlock == Block.Free || destinationBlock == Block.Place)
				{
					boxes.Add(blockDestination, destinationBlock);
					SetBlock(blockDestination, Block.Box);
					if (boxes.TryGetValue(destination, out destinationBlock))
					{
						boxes.Remove(destination);
						SetBlock(destination, destinationBlock);
					}
					else
					{
						// user will see if some error has occurred
						SetBlock(destination, Block.Unknown);
					}
                    return player.Update(destination.X, destination.Y);
				}
			}
			return player;
		}

		public Block GetBlock(int index)
		{
			if (index >= 0 && index < world.Length)
			{
				return (Block)world[index];
			}
			else
			{
				return Block.Unknown;
			}
		}

		public Block GetBlock(Point point)
		{
			return GetBlock(point.Y * Size.X + point.X);
		}

		public void SetBlock(Point point, Block block)
		{
			var index = point.Y * Size.X + point.X;
			if (index >= 0 && index < world.Length)
			{
				world[index] = (byte)block;
			}
		}

		public byte[] GetOriginalMap()
		{
			var array = new byte[world.Length];
			world.CopyTo(array, 0);

			foreach (var item in boxes)
			{
				array[item.Key.Y * Size.X + item.Key.X] = (byte)item.Value;
			}

			foreach (var item in originalBoxPlace)
			{
				array[item.Y * Size.X + item.X] = (byte)Block.Box;
			}
			return array;
		}

		public WorldRaw GetWorld()
		{
			var temp = new WorldRaw();
			temp.size = Size;
			temp.source = GetOriginalMap();
			var state = new Dictionary<Point, Block>();
			foreach (var item in boxes)
			{
				state.Add(item.Key, item.Value);
			}
			temp.state = state;
			return temp;
		}

		public bool IsComplete
		{
			get
			{
				foreach (var item in places)
				{
					if (GetBlock(item) != Block.Box)
					{
						return false;
					}
				}
				return true;
			}
		}

		public static World CreateFromRaw(WorldRaw raw)
		{
			var temp = new World(raw.size, raw.source);
			temp.boxes = raw.state;
			foreach (var item in raw.state)
			{
				temp.SetBlock(item.Key, Block.Box);
			}
			return temp;
		}
	}
}
