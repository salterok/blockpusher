﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace BlockPusher
{
	struct Player
	{
        public int X;
        public int Y;
        public int Steps;
        public string Name;
		public Player(string name, int x, int y)
		{
			X = x;
			Y = y;
            Steps = 0;
            Name = name;
		}

        public Player Update(int x, int y)
        {
            return new Player(Name, x, y)
            {
                Steps = this.Steps + 1
            };
        }
	}
}
