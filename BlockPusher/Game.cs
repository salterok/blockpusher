﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Diagnostics;

namespace BlockPusher
{
	class Game
	{
        Point originalPlayerPosition;

        public Player player { get; private set; }
        public World world { get; private set; }
        public Stopwatch Time { get; private set; }
        public IEnumerable<Highscore> Highscores { get; private set; }

        public Game(World _world, Player _player)
        {
            world = _world;
            player = _player;
            originalPlayerPosition = new Point(_player.X, _player.Y);
            Init();
        }

        public Game()
        {
            Init();
        }

        private void Init()
        {
            Time = new Stopwatch();
            Highscores = new List<Highscore>();
        }

        public void Start()
        {
            Time.Start();
        }

        public void Stop()
        {
            Time.Stop();
        }

        public void Reset()
        {
            Time = new Stopwatch();
            player = new Player()
            {
                Name = this.player.Name,
                X = this.originalPlayerPosition.X,
                Y = this.originalPlayerPosition.Y
            };
            if (world == null)
            {
                return;
            }
            world = new World(world.Size, world.GetOriginalMap());
        }

        public bool IsNotStarted
        {
            get { return this.Time.Elapsed == TimeSpan.Zero; }
        }

        public bool IsStopped
        {
            get
            {
                if (!IsNotStarted)
                {
                    return !Time.IsRunning;
                }
                return true;
            }
        }

		public void Move(Direction direction)
		{
			var destination = new Point(player.X, player.Y);
			switch (direction)
			{
				case Direction.Up:
					destination.Y--;
					break;
				case Direction.Right:
					destination.X++;
					break;
				case Direction.Down:
					destination.Y++;
					break;
				case Direction.Left:
					destination.X--;
					break;
			}
			player = world.MoveTo(player, destination);
		}

		public string Save(string filename = null)
		{
			filename = filename ?? string.Format("{0}.bpf", DateTime.Now.ToString("yyyy-MM-ddThh_mm_ss"));
			var settings = new XmlWriterSettings();
			settings.Indent = true;
			using (var doc = XmlWriter.Create(filename, settings))
			{
				doc.WriteStartDocument(true);
				doc.WriteStartElement("game");

				#region world
				doc.WriteStartElement("world");
				var save = world.GetWorld();
				doc.WriteAttributeString("X", save.size.X.ToString());
				doc.WriteAttributeString("Y", save.size.Y.ToString());
				doc.WriteStartElement("source");
				doc.WriteBase64(save.source, 0, save.source.Length);
				doc.WriteEndElement();
				doc.WriteStartElement("state");
				foreach (var item in save.state)
				{
					doc.WriteStartElement("block");
					doc.WriteAttributeString("X", item.Key.X.ToString());
					doc.WriteAttributeString("Y", item.Key.Y.ToString());
					doc.WriteAttributeString("Value", item.Value.ToString());
					doc.WriteEndElement();
				}
				doc.WriteEndElement();
				doc.WriteEndElement();
				#endregion

				doc.WriteStartElement("player");
				doc.WriteAttributeString("Name", player.Name);
				doc.WriteAttributeString("X", player.X.ToString());
				doc.WriteAttributeString("Y", player.Y.ToString());
				doc.WriteEndElement();
				doc.WriteEndElement();
				doc.WriteEndDocument();
			}
			return filename;
		}

		public void Load(string filename)
		{
			if (!File.Exists(filename))
			{
				return;
			}
			var doc = XDocument.Load(filename);
			var temp = doc.Root.Element("world");
			var point = new Point();
			point.X = Int32.Parse(temp.Attribute("X").Value);
			point.Y = Int32.Parse(temp.Attribute("Y").Value);
			var array = Convert.FromBase64String(temp.Element("source").Value);
			world = new World(point, array);
			foreach (var box in world.boxes)
			{
				world.SetBlock(box.Key, box.Value);
			}
			world.boxes.Clear();
			foreach (var item in temp.Element("state").Elements("block"))
			{
				point.X = Int32.Parse(item.Attribute("X").Value);
				point.Y = Int32.Parse(item.Attribute("Y").Value);
				var block = (Block)Enum.Parse(typeof(Block), item.Attribute("Value").Value);
				world.boxes.Add(point, world.GetBlock(point));
				world.SetBlock(point, Block.Box);
			}
			temp = doc.Root.Element("player");
			player = new Player()
			{
				Name = temp.Attribute("Name").Value,
				X = Int32.Parse(temp.Attribute("X").Value),
				Y = Int32.Parse(temp.Attribute("Y").Value)
			};
            originalPlayerPosition = new Point(player.X, player.Y);
		}

        public int Score
        {
            get
            {
                return (int)(100000 / Time.ElapsedMilliseconds * Math.Pow(player.Steps, 2) / Math.Pow(player.Steps - 1, 2));
            }
        }
	}
}
